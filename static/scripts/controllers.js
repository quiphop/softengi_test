var testApp = angular.module('testApp', ['ui.bootstrap','ngRoute','ui.bootstrap.datetimepicker']);

testApp.config(function($routeProvider, $locationProvider) {
  $routeProvider
   .when('/general', {
    templateUrl: 'static/views/general.html',
    controller: 'testAppController',
  })
  .when('/corrective', {
    templateUrl: 'static/views/corrective.html',
    controller: 'testAppController'
  })

   .when('/submit', {
    templateUrl: 'static/views/submit.html',
    controller: 'testAppController',
 }).otherwise({
        redirectTo: '/general'
      });;
});
testApp.value('corActions', []);
testApp.run(function($rootScope) {
  $rootScope.generalModel = {
                              date:null,
                              reportedBy:'',
                              company:'',
                              supervisor:'',
                              description:'',
                              wellNumber:null,
                              severity:[
                                          {name:"Loss of well controll", checked:false},
                                          {name:"Fatality(ies)", checked:false},
                                          {name:"Hospitalization or medical treatment", checked:false},
                                          {name:"Spill offsite > 50 Bbls", checked:false},
                                          {name:"Spill to water, any amount", checked:false},
                                          {name:"Property damage", checked:false},

                                       ],
                              phone:'',
                              noneApply:false
                             }; 
});
testApp.controller('testAppController',['$scope', '$rootScope','corActions', function ($scope, $rootScope,corActions){
  $scope.companyes = ['companyA','companyB'];
  $scope.wellNumbers = [
                        {well:'Well-01',region:"South",state:"Oklahoma",fieldOffice:"Ringwood"},
                        {well:'Well-02',region:"North",state:"Montana",fieldOffice:"Sidney"},
                        {well:'Well-03',region:"North",state:"North Dakota",fieldOffice:"Tioga"}
                       ];
  $scope.currentWell = null;
  $scope.incSeverity = [
                        {name:"Loss of well controll", checked:false},
                        {name:"Fatality(ies)", checked:false},
                        {name:"Hospitalization or medical treatment", checked:false},
                        {name:"Spill offsite > 50 Bbls", checked:false},
                        {name:"Spill to water, any amount", checked:false},
                        {name:"Property damage", checked:false}
                      ];

  $scope.corAction = {
    description:"",
    takenBy:"",
    company:"",
    date:""
  };

  $rootScope.corActions = corActions;
  // $scope.noneApply = false;
  $scope.main_tabs = [
      { link : '#/general', label : 'General information' },
      { link : '#/corrective', label : 'Corrective Actions' },
      { link : '#/submit', label : 'REVIEW and SUBMIT' }
    ]; 
  $scope.selectedTab = $scope.main_tabs[0];
  
  $scope.setSelectedTab = function(tab) {
    $scope.selectedTab = tab;
  };
  
//DATEPICKER controller
  $scope.today = function() {
    $scope.dt = new Date();
  };

  $scope.clear = function () {
    $scope.dt = null;
  };

  $scope.disabled = function(date, mode) {
    return ( mode === 'day' && ( date.getDay() === 0 || date.getDay() === 6 ) );
  };

  $scope.toggleMin = function() {
    $scope.minDate = $scope.minDate ? null : new Date();
  };
  $scope.toggleMin();
 
  $scope.open = function($event) {
    $event.preventDefault();
    $event.stopPropagation();
    $scope.opened = true;
  };

  $scope.dateOptions = {
    formatYear: 'yy',
    startingDay: 1
  };

  $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
  $scope.format = $scope.formats[2];
  var tomorrow = new Date();
  tomorrow.setDate(tomorrow.getDate() + 1);
  var afterTomorrow = new Date();
  afterTomorrow.setDate(tomorrow.getDate() + 2);
  $scope.events =
    [
      {
        date: tomorrow,
        status: 'full'
      },
      {
        date: afterTomorrow,
        status: 'partially'
      }
    ];

  $scope.getDayClass = function(date, mode) {
    if (mode === 'day') {
      var dayToCheck = new Date(date).setHours(0,0,0,0);

      for (var i=0;i<$scope.events.length;i++){
        var currentDay = new Date($scope.events[i].date).setHours(0,0,0,0);

        if (dayToCheck === currentDay) {
          return $scope.events[i].status;
        }
      }
    }

    return '';
  };
  //END DATEPICKER controller

  $scope.addCorAction = function (description,takenBy,company,date){
    console.log($scope.corAction);
    corActions.push({description:description,takenBy:takenBy,company:company,date:date});
    $scope.corAction = {
      description:"",
      takenBy:"",
      company:"",
      date:""
    };
  };

  $scope.removeAction = function(index){
    console.log(index);
    corActions.splice(index,1);
  };

  $scope.checkSeverity = function(){
    var result = false;
    for(var check in $rootScope.generalModel.severity){
      if($rootScope.generalModel.severity[check].checked){
        result = true;
      }
      if($rootScope.generalModel.noneApply)result = true;
    }
    return result;
  };

  $scope.checkSubmit = function(){
    if(!$rootScope.generalModel.date || 
       !$rootScope.generalModel.reportedBy ||
       !$rootScope.generalModel.company ||  
       !$rootScope.generalModel.description ||
       !$rootScope.generalModel.wellNumber ||
       !$rootScope.generalModel.phone ||
       !$scope.phoneRegex ||
       !$scope.checkSeverity() || 
       $rootScope.corActions.length == 0
       )return true;
      else return false;
  };

  $scope.submit = function(){
    var responseObject={
                        "workflowCreationInformation":{
                            "workflowTypeName": "Incident Report",
                            "name": "Report - 2013.05.09"
                        },
                        "workflowStepUpdateInformation":{
                            "stepIdOrName": "Initial Step",
                            "fields":[]
                        }
                    },
    sevArray = [];
    for(var s in $rootScope.generalModel.severity){
      if($rootScope.generalModel.severity[s].checked)sevArray.push($rootScope.generalModel.severity[s].name);
    }

    responseObject.workflowStepUpdateInformation.fields.push( 
                                {"name":"Date and Time of Incident","values":[$rootScope.generalModel.date]},
                                {"name":"Reported By","values":[$rootScope.generalModel.reportedBy]},
                                {"name":"Company of Reporter","values":[$rootScope.generalModel.company]},
                                {"name":"Contact Number","values":[$rootScope.generalModel.phone]},
                                {"name":"Supervisor Name","values":[$rootScope.generalModel.supervisor]},
                                {"name":"High Level Description of Incident","values":[$rootScope.generalModel.description]},
                                {"name":"Well Number","values":[$rootScope.generalModel.wellNumber.well]},
                                {"name":"Region","values":[$rootScope.generalModel.wellNumber.region]},
                                {"name":"State","values":[$rootScope.generalModel.wellNumber.state]},
                                {"name":"Field Office","values":[$rootScope.generalModel.wellNumber.fieldOffice]},
                                {"name":"Incident Severity (Check all that Apply)","values":sevArray}
                              );
    for(var c in $rootScope.corActions){
        responseObject.workflowStepUpdateInformation.fields.push(
          {"name":"Description of Corrective Action" + "("+c+")","values":[$rootScope.corActions[c].description]},
          {"name":"Action Taken By (name) "+ "("+c+")","values":[$rootScope.corActions[c].takenBy]},
          {"name":"Company"+ "("+c+")","values":[$rootScope.corActions[c].company]},
          {"name":"Date"+ "("+c+")","values":[$rootScope.corActions[c].date]}
        );
     }
    var response = window.open("data:text/json," + JSON.stringify(responseObject),
                       "_blank");
    response.focus();
  };

  $scope.phoneRegex = function(){
    var pattern = /^\(?([0-9]{3})\)?[.. ]?([0-9]{3})[.. ]?([0-9]{4})$/;
    return pattern.test($rootScope.generalModel.phone);
  };

}]);
